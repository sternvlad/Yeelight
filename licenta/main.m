//
//  main.m
//  licenta
//
//  Created by Edi Stern on 6/12/18.
//  Copyright © 2018 Edi Stern. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
