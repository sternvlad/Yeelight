//
//  ViewController.m
//  licenta
//
//  Created by Edi Stern on 6/12/18.
//  Copyright © 2018 Edi Stern. All rights reserved.
//

#import "ViewController.h"
#import "Socket.h"
#import "Device.h"
#import "OpenViewController.h"
@interface ViewController()<UITableViewDelegate,UITableViewDataSource>
@property (weak) IBOutlet UITableView *tableView;
@property (strong,nonatomic)NSArray *dataSource;
@property (weak) IBOutlet UIView *loading;
@property (strong,nonatomic)Device *selectDevice;
@end

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.dataSource = @[];
    // show progress view
    self.loading.hidden = NO;
    // searching for devices
    [[Socket sharedInstance] searchDevice];
    // every one second is searching again for devices
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(getDevices) userInfo:nil repeats:YES];
}


- (void)getDevices{
    // get devices in an array
    NSDictionary *devices = [[Socket sharedInstance] getDevices];
    self.dataSource = [devices allValues];
    // reload table view to display the devices
    [self.tableView reloadData];
    // hide progress view
    self.loading.hidden = YES;
    if (self.dataSource.count< 1) {
    }else {
        NSLog(@"123123213");
    }
}

// functions to implement tableview

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:CellIdentifier];
    }
    Device *device = [self.dataSource objectAtIndex:indexPath.row];
    // Configure the cell.
    NSString *color=@"";
    NSString *deviceModel=@"";
    if ([device.model isEqualToString: @"color"]) {
        color = @"Color";
    }else if ([device.model isEqualToString:@"mono"]){
        color = @"Mono";
    }else{
        color = @"Unknown";
    }
    deviceModel = [NSString stringWithFormat:@"ModelID: %@ Location: %@", device.model, device.location];
    cell.textLabel.text = deviceModel;
    cell.detailTextLabel.text = color;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectDevice =  [self.dataSource objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"segueToControl" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"segueToControl"])
    {
        // Get reference to the destination view controller
        OpenViewController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        [vc setDevice:self.selectDevice];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
