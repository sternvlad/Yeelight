//
//  OpenViewController.m
//  licenta
//
//  Created by Edi Stern on 6/12/18.
//  Copyright © 2018 Edi Stern. All rights reserved.
//

#import "OpenViewController.h"

@interface OpenViewController ()

@end

@implementation OpenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // set the color of button for on/off light bulb
    [self.onOffSwitch setSelected:self.device.isOn];
    [self.device changeBrightness:50];
    // if device is not connected you should be kicked out
    if (![self.device connect]) {
        NSLog(@"some things err");
       // [self.navigationController popViewControllerAnimated:YES];
    }
    // Do any additional setup after loading the view.
}

// function to power on or off the bulb
- (IBAction)valueChanged:(UIButton *)btn {
    [self.device swichtLight:!btn.selected];
    btn.selected = !btn.selected;
}

// function to change brithness and display it in a label, minimum it's 0, maximum it's 100
- (IBAction)brightnessChange:(UISlider*)sender {
    int discreteValue = roundl([sender value]);
    [self.device changeBrightness:sender.value];
    self.lblBrithness.text = [NSString stringWithFormat:@"Britght: %d",(int)discreteValue];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// change temperature if you had a device
- (IBAction)temperatureChange:(UISlider*)sender {
    int discreteValue = roundl([sender value]);
    self.lblTemperature.text = [NSString stringWithFormat:@"Temp: %d",(int)discreteValue];
}

@end
