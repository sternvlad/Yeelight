//
//  WebViewController.h
//  licenta
//
//  Created by Edi Stern on 13/06/2018.
//  Copyright © 2018 Edi Stern. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate>
@property(weak, nonatomic) IBOutlet UIWebView *webView;
@property(weak, nonatomic) IBOutlet UIView *loading;

@end
