//
//  WebViewController.m
//  licenta
//
//  Created by Edi Stern on 13/06/2018.
//  Copyright © 2018 Edi Stern. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // load local html file into browser
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"informatii" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [_webView loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    //hide loading when web view was loaded
    _loading.hidden = YES;
}


@end
