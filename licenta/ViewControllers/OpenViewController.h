//
//  OpenViewController.h
//  licenta
//
//  Created by Edi Stern on 6/12/18.
//  Copyright © 2018 Edi Stern. All rights reserved.
//

#import "ViewController.h"
#import "Device.h"

@interface OpenViewController : ViewController
@property(strong,nonatomic)Device *device;
@property(weak, nonatomic) IBOutlet UIButton *onOffSwitch;
@property(weak, nonatomic) IBOutlet UISlider *brithnessSlide;
@property(weak, nonatomic) IBOutlet UILabel *lblTemperature;
@property(weak, nonatomic) IBOutlet UILabel *lblBrithness;
@end
