//
//  AppDelegate.h
//  licenta
//
//  Created by Edi Stern on 6/12/18.
//  Copyright © 2018 Edi Stern. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

